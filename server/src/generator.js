const faker = require('faker');
const fs = require('fs');

const randomID = () => {
  return Math.floor(Math.random(1) * 100000) + 99999;
};

const randomTodoList = (number) => {
  if (number < 0) return [];

  const todolist = [];

  for (let i = 0; i < number; i++) {
    const todo = {
      id: randomID(),
      title: faker.name.jobTitle(),
      description: faker.commerce.productDescription(),
      status: faker.random.boolean(),
      createdAt: new Date().getTime(),
      updatedAt: new Date().getTime(),
    };
    todolist.push(todo);
  }

  return todolist;
};

(() => {
  const todoList = randomTodoList(10);

  const db = {
    todos: todoList,
    profile: {
      name: 'Vinh Nguyen',
    },
  };

  // Write DB object to db.json
  fs.writeFile('./src/db.json', JSON.stringify(db), () => {
    console.log('Generate data successfully.');
  });
})();
