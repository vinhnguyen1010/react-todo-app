import axios from '../helps/http-common';

const URI = `todos`;

const getTodos = (params) => {
  return axios.get(URI, { params });
};

const addTodo = (body) => {
  return axios.post(URI, body);
};

const removeTodo = (id) => {
  return axios.delete(`${URI}/${id}`);
};

const editTodo = (id, todo) => {
  return axios.patch(`${URI}/${id}`, todo);
};

export default {
  getTodos,
  addTodo,
  removeTodo,
  editTodo,
};
