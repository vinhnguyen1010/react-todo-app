import { grey, red, teal } from '@material-ui/core/colors';
import { createTheme } from '@material-ui/core/styles';

const themeDefault = createTheme({
  palette: {
    primary: {
      main: red[400],
      dark: red[900],
      light: red[200],
      contrastText: '#FFFFFF',
    },
    secondary: {
      main: teal[500],
      dark: teal[900],
      light: teal[300],
      contrastText: '#FFFFFF',
    },
  },

  
  typography: {
    body1: {
      fontWeight: 400,
      // fontSize: '1.35rem',
    },
  },
});

export default themeDefault;
