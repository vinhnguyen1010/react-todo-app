import {
  AppBar,
  Box,
  CssBaseline,
  List,
  ListItem,
  ListItemText,
  makeStyles,
  Toolbar,
  Typography,
} from '@material-ui/core';
import React, { Suspense } from 'react';
import { BrowserRouter as Router, NavLink, Route, Switch } from 'react-router-dom';
import { LoadingSpinner } from '../../containers/Loading/LoadingSpinner';
import { NavLinks } from '../../Navigation/NavLinks';
import { Routes } from '../../Navigation/Routes/Routes';
import theme from '../../themes/theme';

const primary = theme.palette.primary;
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    
  },

  
  breakpoints: { ...theme.breakpoints.values },

  appBar: {
    height: '4rem',
    borderBottom: `1px solid ${theme.palette.divider}`,
  },

  toolbar: {
    flexWrap: 'wrap',
    display: 'flex',
    flexDirection: 'row',
    padding: theme.spacing(0, 5),
    ...theme.mixins.toolbar,
  },

  toolbarTitle: {
    flexGrow: 1,
    cursor: `pointer`,
    flexDirection: 'row',
    textTransform: `uppercase`,
    letterSpacing: `0.02rem`,
    fontSize: '1.2rem',
  },

  navlinks: {
    display: 'flex',
    flexGrow: 1,
    flexWrap: 'wrap',
    justifyContent: 'center',
  },

  navDisplayFlex: {
    display: 'flex',
    justifyContent: `space-between`,
    padding: theme.spacing(0),
  },

  navItems: {
    textDecoration: `none`,
    textTransform: `uppercase`,
    [theme.breakpoints.up('md')]: {
      margin: '0 5px',
    },

    '&.active': {
      backgroundColor: primary.light,
      borderBottom: '2px solid #FFFFFF',
    },

    '&:hover': {
      cursor: `pointer`,
      backgroundColor: primary.light,
      borderBottom: '2px solid #FFFFFF',
    },
    '&:focus': {
      backgroundColor: primary.dark,
    },
  },

  linkItem: {
    display: `flex`,
    flexGrow: 1,
    justifyContent: `space-between`,
    textDecoration: `none`,
    padding: theme.spacing(1.75, 1),
  },

  content: {
    top: '4rem',
    display: 'block',
    position: 'absolute',
    width: '100%',
    flex: '1 1 auto',
    overflow: 'hidden',
    // margin: '4rem 1.5rem',
    padding: ' 0 2rem',
    
    [theme.breakpoints.up('md')]: {
      // margin: '0 1rem',
      padding: '1rem 2.5rem',
    },
  },

  // heroContent: {
  //   padding: theme.spacing(8, 0, 6),
  // },

  footer: {
    borderTop: `1px solid ${theme.palette.divider}`,
    marginTop: theme.spacing(8),
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3),
    [theme.breakpoints.up('sm')]: {
      paddingTop: theme.spacing(6),
      paddingBottom: theme.spacing(6),
    },
  },
}));

const Header = (props) => {
  const navLinks = NavLinks;
  const routes = Routes;
  const classes = useStyles();

  return (
    <>
      <Router>
        <CssBaseline />
        <AppBar position="fixed" color="primary" elevation={1} className={classes.appBar}>
          <Toolbar className={classes.toolbar}>
            <Typography variant="body1" className={classes.toolbarTitle} noWrap>
              React Web Todo
            </Typography>
            <Box flexGrow={8} />
            <div className="navlinks">
              <List
                component="nav"
                aria-labelledby="main navigation"
                className={classes.navDisplayFlex}
              >
                {navLinks.map((item, index) => (
                  <NavLink className={classes.navItems} exact={true} to={item.path} key={index}>
                    <ListItem button className={classes.linkItem} key={item.title}>
                      <ListItemText primary={item.title} />
                    </ListItem>
                  </NavLink>
                ))}
              </List>
            </div>
          </Toolbar>
        </AppBar>

        {/* ------------------------ */}
        <div className={classes.content}>
          <Suspense fallback={<LoadingSpinner />}>
            <Switch>
              {routes.map((route, index) => (
                <Route exact={true} key={index} path={route.path} component={route.component} />
              ))}
            </Switch>
          </Suspense>
        </div>
      </Router>
    </>
  );
};

export default Header;
