import React from 'react';
import { ToastifyContextProvider } from '../../contexts/ToastifyContext';
import { TodoContextProvider } from '../../contexts/TodoContext';
import TodoList from './container/TodoList';

// const ListTodo = () => {

// }

const Todos = () => {
  return (
    <>
      <TodoContextProvider>
        <ToastifyContextProvider>
          <TodoList />
        </ToastifyContextProvider>
      </TodoContextProvider>
    </>
  );
};

export default Todos;
