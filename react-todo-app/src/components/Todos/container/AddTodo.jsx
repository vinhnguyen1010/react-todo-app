import { Button, Divider } from '@material-ui/core';
import React from 'react';

const AddTodo = (props) => {
  const { inputTodo, onChangeInputTodo, onSubmitTodo } = props;


  return (

    <>
      <h5>Add New Todo</h5>
      <Divider />
      <form onSubmit={onSubmitTodo}>
        <div className="my-3">
          <label htmlFor="title" className="form-label">
            Title todo
          </label>
          <input
            type="text"
            className="form-control"
            id="TitleTodo"
            autoComplete="off"
            placeholder="title todo"
            name="title"
            value={inputTodo.title}
            onChange={onChangeInputTodo}
          />
        </div>

        <div className="mb-3">
          <label htmlFor="description" className="form-label">
            Description todo
          </label>
          <textarea
            type="text"
            className="form-control"
            id="description"
            autoComplete="off"
            placeholder="Description"
            name="description"
            value={inputTodo.description}
            onChange={onChangeInputTodo}
          />
        </div>

        <div className="mb-3">
          <Button variant="contained" color="primary" onClick={onSubmitTodo}>
            Submit
          </Button>
         
        </div>
      </form>
    </>
  );
};

export default AddTodo;
