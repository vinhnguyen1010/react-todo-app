import { Divider } from '@material-ui/core';
import React, { useContext, useState } from 'react';
import { TodoContext } from '../../../contexts/TodoContext';
import TodoItem from './TodoItem';
import './Todo.scss';
import EditTodo from './EditTodo';
import AddTodo from './AddTodo';
import { LoadingSpinner } from '../../../containers/Loading/LoadingSpinner';
import { ToastifyContext } from '../../../contexts/ToastifyContext';

const TodoList = () => {
  const initialInputState = {
    title: '',
    description: '',
    status: false,
  };

  const [isLoading, setIsLoading] = useState(false);
  const [isEditing, setIsEditing] = useState(false);
  const [inputTodo, setInputTodo] = useState(initialInputState);
  const [currentTodo, setCurrentTodo] = useState({});

  const { todos, createTodo, updateTodo, deleteTodo } = useContext(TodoContext);
  const { messageNotify } = useContext(ToastifyContext);

  // handle input todo form add
  const handleChangeInputTodo = (e) => {
    const { name, value } = e.target;
    setInputTodo({
      ...inputTodo,
      [name]: value,
    });
  };

  // hanlde submit data form
  const handleSubmitTodo = (e) => {
    setIsLoading(true);

    e.preventDefault();
    if (!initialInputState) return;

    createTodo(inputTodo)
      .then((res) => {
        // console.log(res);
        setInputTodo(initialInputState);
        messageNotify('success', res.statusText);
        setIsLoading(false);
      })
      .catch((error) => {
        console.log(error);
        setIsLoading(false);
      });
  };

  const handleChangedInputTodoEdit = (e) => {
    const { name, value } = e.target;
    setCurrentTodo({
      ...currentTodo,
      [name]: value,
    });
  };

  const handleEditTodo = (todo) => {
    setIsEditing(true);
    setCurrentTodo({ ...todo });
  };

  const handleUpdateTodo = (e) => {
    // console.log('data update===', currentTodo);
    setIsLoading(true);

    e.preventDefault();
    if (!currentTodo) return;
    updateTodo(currentTodo.id, currentTodo)
      .then((res) => {
        // console.log('~ res', res);
        setCurrentTodo(initialInputState);
        setIsEditing(false);
        setIsLoading(false);
        messageNotify('success', `Updated is ${res.statusText}`);
        return res.data;
      })
      .catch((error) => {
        setIsLoading(false);
        return error;
      });
  };

  const handleDeleteTodo = (todo) => {
    setIsLoading(true);

    deleteTodo(todo)
      .then((res) => {
        // console.log(res);
        setIsLoading(false);
        messageNotify('success', `Deleted is ${res.statusText}`);
        return res;
      })
      .catch((error) => {
        setIsLoading(false);
        return error;
      });
  };

  return (
    <div className="row">
      {isLoading ? <LoadingSpinner /> : ' '}
      <div className="col-5">
        {isEditing === true ? (
          <EditTodo
            currentTodoEdit={currentTodo}
            setIsEditing={setIsEditing}
            changeInputTodoEdit={handleChangedInputTodoEdit}
            onUpdateTodo={handleUpdateTodo}
          />
        ) : (
          <AddTodo
            inputTodo={inputTodo}
            onChangeInputTodo={handleChangeInputTodo}
            onSubmitTodo={handleSubmitTodo}
          />
        )}
      </div>

      {/* Todo list */}
      {isLoading ? <LoadingSpinner /> : ''}
      <div className="col-7">
        <h5>List Todo</h5>
        <Divider />
        {todos.length > 0 ? (
          <div className="item-todos">
            <ul className="list-group list-item-todos">
              {todos.map((todo, index) => (
                <TodoItem
                  todo={todo}
                  key={index}
                  onEditTodo={() => handleEditTodo(todo)}
                  onDeleteTodo={() => handleDeleteTodo(todo)}
                />
              ))}
            </ul>
          </div>
        ) : (
          <div className="my-3 d-flex justify-content-center">
            <h3> No data Todo </h3>
          </div>
        )}
      </div>
    </div>
  );
};

export default TodoList;
