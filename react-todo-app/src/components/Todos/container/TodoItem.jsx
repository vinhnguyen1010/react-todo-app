import { Button, Checkbox, Collapse, IconButton } from '@material-ui/core';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { ExpandLess, ExpandMore } from '@material-ui/icons';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import React, { createRef, useContext, useState } from 'react';
import { LoadingSpinner } from '../../../containers/Loading/LoadingSpinner';
import { TodoContext } from '../../../contexts/TodoContext';
import './Todo.scss';

const TodoItem = (props) => {
  const { todo, onEditTodo, onDeleteTodo } = props;

  const [isLoading, setIsLoading] = useState(false);

  const { updateTodo } = useContext(TodoContext);
  const [isCheckStatus, setIsCheckStatus] = useState(todo.status);
  const [textMessage, setTextMessage] = useState('');
  const [openDesciption, setOpenDesciption] = useState(false);
  const [openDialog, setOpenDialog] = useState(false);
  const dialogRef = createRef();

  const handleDescriptionClick = () => {
    setOpenDesciption(!openDesciption);
  };

  const handleChangeStatusClick = (e) => {
    setIsCheckStatus(e.target.checked);
    const todoItem = {
      ...todo,
      [e.target.name]: !isCheckStatus,
    };
    setIsLoading(true);

    // console.log(' ~ todoItem', todoItem);
    updateTodo(todoItem.id, todoItem)
      .then((res) => {
        setIsLoading(false);

        return res;
      })
      .catch((error) => {
        setIsLoading(false);
        return error;
      });
  };

  const handleOpenDialog = (todo) => {
    setOpenDialog(true);
    const message = 'Are you sure watn to delete todo: ' + todo.title + '?';
    setTextMessage(message);
  };

  const closeDialog = () => {
    setOpenDialog(false);
  };

  const handleDeleteTodo = (todo) => {
    onDeleteTodo(todo);
    setOpenDialog(false);
  };

  const DialogConfirmDelete = (message) => {
    return (
      <Dialog maxWidth="xs" onClose={closeDialog} open={openDialog} ref={dialogRef}>
        <DialogTitle onClose={closeDialog}>{'Confirm Delete Todo?'}</DialogTitle>
        <DialogContent dividers>
          <DialogContentText>{message}</DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button color="primary" autoFocus onClick={() => handleDeleteTodo(todo)}>
            Delete
          </Button>
          <Button onClick={closeDialog} color="default">
            Cancel
          </Button>
        </DialogActions>
      </Dialog>
    );
  };

  return (
    <div>
      {isLoading ? <LoadingSpinner /> : ''}
      <li
        className={
          todo.status === true
            ? 'todo-item list-group-item-success'
            : 'todo-item list-group-item-primary'
        }
      >
        <div className="d-flex align-items-center justify-content-between">
          <div>
            <Checkbox
              color="primary"
              name="status"
              value={isCheckStatus}
              checked={isCheckStatus}
              onChange={(e) => handleChangeStatusClick(e)}
            />
            <span className={todo.status ? 'mx-1 item-todo-status-finished' : 'mx-1'}>
              {todo.title}{' '}
            </span>
          </div>

          <span>
            <IconButton onClick={handleDescriptionClick}>
              {openDesciption ? <ExpandLess /> : <ExpandMore />}
            </IconButton>
            <IconButton aria-label="edit" onClick={() => onEditTodo(todo)}>
              <EditIcon />
            </IconButton>
            <IconButton aria-label="delete" onClick={() => handleOpenDialog(todo)}>
              <DeleteIcon />
            </IconButton>

            {DialogConfirmDelete(textMessage)}
          </span>
        </div>

        <Collapse in={openDesciption} timeout="auto" unmountOnExit>
          <span className="p-2">{todo.description}</span>
        </Collapse>
      </li>
    </div>
  );
};

export default TodoItem;
