import { Button, Divider } from '@material-ui/core';
import React from 'react';

const EditTodo = (props) => {
  const { currentTodoEdit, setIsEditing, changeInputTodoEdit, onUpdateTodo } = props;

  return (
    <>
      <h5>Update Todo</h5>
      <Divider />
      <form onSubmit={onUpdateTodo}>
        <div className="my-3">
          <label htmlFor="title" className="form-label">
            Title todo
          </label>
          <input
            type="text"
            className="form-control"
            id="TitleTodo"
            autoComplete="off"
            placeholder="title todo"
            name="title"
            value={currentTodoEdit.title}
            onChange={changeInputTodoEdit}
          />
        </div>

        <div className="mb-3">
          <label htmlFor="description" className="form-label">
            Description todo
          </label>
          <textarea
            type="text"
            className="form-control"
            id="description"
            autoComplete="off"
            placeholder="Description"
            name="description"
            value={currentTodoEdit.description}
            onChange={changeInputTodoEdit}
          />
        </div>

        <div className="mb-3">
          <Button variant="contained" color="primary" onClick={onUpdateTodo}>
            Save
          </Button>

          <Button
            className="mx-3"
            variant="contained"
            color="default"
            onClick={() => setIsEditing(false)}
          >
            Cancel
          </Button>
        </div>
      </form>
    </>
  );
};

export default EditTodo;
