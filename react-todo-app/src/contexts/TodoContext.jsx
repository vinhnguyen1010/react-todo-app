import { createContext, useEffect, useReducer } from 'react';
import { ADD_TODO, EDIT_TODO, GET_TODOS, REMOVE_TODO } from '../reducers/action-types';
import { todoReducers } from '../reducers/todoReducer';
import TodosService from '../services/Todos.service';

export const TodoContext = createContext();

export const TodoContextProvider = ({ children }) => {
  const [todos, dispatch] = useReducer(todoReducers, []);

  const fetchTodo = async () => {
    return TodosService.getTodos().then((res) => {
      dispatch({
        type: GET_TODOS,
        payload: res.data,
      });
      return res.data;
    });
  };

  const createTodo = async (todo) => {
    return TodosService.addTodo(todo).then((res) => {
      dispatch({
        type: ADD_TODO,
        payload: res.data,
      });
      return res;
    });
  };

  const deleteTodo = async (todo) => {
    return TodosService.removeTodo(todo.id)
      .then((res) => {
        dispatch({
          type: REMOVE_TODO,
          payload: todo,
        });
        return res;
      })
      .catch((error) => {
        return error;
      });
  };

  const updateTodo = async (id, newTodo) => {
    return TodosService.editTodo(id, newTodo)
      .then((res) => {
        dispatch({
          type: EDIT_TODO,
          payload: res.data,
        });
        return res;
      })
      .catch((error) => {
        return error;
      });
  };

  useEffect(() => {
    //
    fetchTodo();

    return () => {
      // cleanup
    };
  }, []);

  const dataTodoContext = {
    todos,
    createTodo,
    fetchTodo,
    deleteTodo,
    updateTodo,
    dispatch,
  };

  return <TodoContext.Provider value={dataTodoContext}>{children}</TodoContext.Provider>;
};
