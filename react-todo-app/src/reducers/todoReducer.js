import { ADD_TODO, REMOVE_TODO, GET_TODOS, EDIT_TODO } from './action-types';

const initialState = {
  todos: [],
  todoId: null,
  loading: true,
};
export const todoReducers = (state = [], action) => {
  const { type, payload } = action;

  switch (type) {
    case ADD_TODO:
      return [...state, payload];

    case GET_TODOS:
      return payload;

    case REMOVE_TODO:
      return state.filter((todo) => todo.id !== payload.id);

    case EDIT_TODO:
      return state.map((todo) => {
        if (todo.id === payload.id) {
          return payload;
        }
        return todo;
      });

    default:
      return state;
  }
};
