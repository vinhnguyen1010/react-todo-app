export const GET_TODOS = 'GET_TODOS';
export const ADD_TODO = 'ADD_TODO';
export const REMOVE_TODO = 'DELETE_TODO';
export const EDIT_TODO = 'EDIT_TODO';
