import { lazy } from 'react';

export const Routes = [
  {
    path: ['/', '/todos'],
    component: lazy(() => import('../../components/Todos/Todos')),
  },
  {
    path: '/about',
    component: lazy(() => import('../../components/About/About')),
  },
  {
    path: '*',
    component: lazy(() => import('../../components/Exception/NotFound')),
  },
];
