import { ThemeProvider } from '@material-ui/core';
import './App.scss';
import Header from './components/Header/Header';
import './styles/styles.scss';
import theme from './themes/theme';

function App() {
  return (
    // <div className="App">
    //   {/* <header className="App-header">
    //     <img src={logo} className="App-logo" alt="logo" />
    //   </header>
    //    */}
    // </div>

    <ThemeProvider theme={theme}>
      <Header />
    </ThemeProvider>
  );
}

export default App;
